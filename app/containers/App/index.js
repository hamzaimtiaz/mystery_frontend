import React, { useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
// import '../../assets/static/home.css';
// import HomePage from 'containers/HomePage/Loadable';
import Login from 'containers/Login/Loadable';
import Signup from 'containers/Signup/Loadable';
// import Trigger from 'containers/Trigger/Loadable';
// import Menu from 'containers/Menu/Loadable';
// import Header from 'components/Header';
import { createStructuredSelector } from 'reselect';
// import Footer from 'components/Footer';
import { connect } from 'react-redux';
import { makeSelectAuth, makeSelectQuestions } from './selectors';
import { useInjectReducer } from '../../utils/injectReducer';
import reducer from './reducer';
import { useInjectSaga } from '../../utils/injectSaga';
import saga from './saga';
import { fetchUserToken } from './actions';
import HomePage from '../HomePage';
import FormsList from '../FormsList';
import FormEdit from '../FormEdit';

const key = 'app';

function App({ dispatchFetchUserToken  }) {
  // authentication
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    dispatchFetchUserToken();
  }, []);
  return (
    <div className="mainStyle">
      <div>{/* <Header /> */}</div>
        <Switch>
          <Route path="/Home" component={HomePage} />
          <Route path="/Forms" component={FormsList} />
          <Route path="/UpdateForm" component={FormEdit} />
          <Route path="/signup" component={Signup} />
          <Route path="/" component={Login} />
        </Switch>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  authentication: makeSelectAuth(),
  // questions : makeSelectQuestions(),
});

const mapDispatchToProps = dispatch => ({
  dispatchFetchUserToken: data => dispatch(fetchUserToken(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
