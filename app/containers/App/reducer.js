import produce from 'immer';
import { QUESTION_INFO_SUCCESS } from './constants';
import { FETCH_TOKEN_SUCCESS } from '../Login/constants';

export const initialState = {
  authentication: {},
  questions: [],
};

/* eslint-disable default-case, no-param-reassign */

const appReducer = (state = initialState, action) =>
  produce(state, draft => {
    // eslint-disable-next-line default-case
    switch (action.type) {
      // case FETCH_USER_SUCCESS:
      //   draft.authentication = action.payload;
      //   break;

      case FETCH_TOKEN_SUCCESS:
        draft.authentication = action.payload;
        break;

      // case LOGOUT:
      //   draft.authentication = {};
      //   break;
    }
  });

export default appReducer;
