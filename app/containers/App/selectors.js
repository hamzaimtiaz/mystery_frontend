import { createSelector } from 'reselect';
import _ from 'lodash';
import { initialState } from './reducer';
const selectGlobal = state => state.app || initialState;

export const makeSelectAuth = () =>
  createSelector(
    selectGlobal,
    globalState =>
      _.isEmpty(globalState.authentication) ? {} : globalState.authentication,
  );
export const makeSelectQuestions = () =>
createSelector(
  selectGlobal,
  app => app.questions,
);

