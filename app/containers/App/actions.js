import {
  FETCH_USER_TOKEN,
  FETCH_USER_SUCCESS,
  FETCH_QUESTION_INFO,
  QUESTION_INFO_SUCCESS,
  QUESTION_INFO_FAILED,
  CLEAR_STORAGE,
  LOGOUT,
} from './constants';

export function fetchUserToken() {
  return {
    type: FETCH_USER_TOKEN,
  };
}

export function userLoaded(payload) {
  return {
    type: FETCH_USER_SUCCESS,
    payload,
  };
}

export function clearSession() {
  return {
    type: CLEAR_STORAGE,
  };
}

export function logout() {
  return {
    type: LOGOUT,
  };
}



