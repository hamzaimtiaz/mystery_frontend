import { call, put, takeLatest } from 'redux-saga/effects';
import { CLEAR_STORAGE, FETCH_USER_TOKEN } from './constants';
import { FETCH_TOKEN } from '../Login/constants';
import { API_URL,TOKEN_API } from '../../config';
import request from '../../utils/request';
import { tokenFailed, tokenLoaded } from '../Login/actions';
import { logout } from './actions';

export function* getToken(login) {
  const requestURL = `${API_URL}${TOKEN_API}`;

  try {
    const tokenResponse = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(login.login),
    });

    yield put(tokenLoaded(tokenResponse));
    localStorage.setItem('authentication', JSON.stringify(tokenResponse));
  } catch (err) {
    yield put(tokenFailed(err));
  }
}

export function* getUserToken() {
  try {
    const authentication = localStorage.getItem('authentication');
    if (authentication) {
      yield put(tokenLoaded(JSON.parse(authentication)));
    }
  } catch (err) {}
}

export function* clearStorage() {
  try {
    localStorage.removeItem('authentication');
    yield put(logout());
  } catch (err) {}
}


export default function* appSaga() {
  yield takeLatest(FETCH_USER_TOKEN, getUserToken);
  yield takeLatest(CLEAR_STORAGE, clearStorage);
  yield takeLatest(FETCH_TOKEN, getToken);
  
}
