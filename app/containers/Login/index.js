import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { fetchToken } from './actions';
import { makeSelectAuth } from '../App/selectors'
import rightLogo from '../../assets/images/right_logo.svg';
import logo from '../../assets/images/logo.png';

function Login({ dispatchLoadToken,auth,history }) {

  if (auth.access) {
    history.push('/Home');
  }


  const [login, setForm] = useState({});

  const handleOnChange = ev => {
    setForm({ ...login, [ev.target.name]: ev.target.value });
  };

  const handleOnSubmit = () => {
    dispatchLoadToken(login);
  };

  return (
    <div className="d-flex flex-row main-home">
      <div className="flex-fill w-50 bg-white">
          <div className="container login bg-white">
            <div className=" inner-login">
            <img className='mt-5 ' src={logo} alt=""></img>
            <form className="login-form">
              <div className="form-group">
                <input
                type="text"
                name="username"
                id="username"
                className="form-control input-field"
                placeholder="Enter your email"
                onChange={handleOnChange}
                value={useState.username}
                />
              </div>
              <div className="form-group">
                <input
                type="password"
                name="password"
                id="password"
                className="form-control input-field"
                placeholder="Password"
                onChange={handleOnChange}
                value={useState.password}
                />
              </div>
              <div className="form-group">
                <button
                type="button"
                name="submit"
                className="btn btn-md login-btn input-field"
                onClick={handleOnSubmit}
                >
                Login
                </button>
              </div>
            </form>
            </div>
          </div>
        </div>
      <div className="flex-fill w-50 d-none d-md-block right-logo">
        <div className='right-side'>
          <img src = {rightLogo} alt=""/>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  auth : makeSelectAuth()
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadToken: login => dispatch(fetchToken(login)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(Login);
