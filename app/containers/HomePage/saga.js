import { call, put, takeLatest } from 'redux-saga/effects';
import { API_URL, USER_API,QUESTION_API, FORM_API, ANSWERED_API } from '../../config';
import request from '../../utils/request';
import {
  FETCH_USER_TITLE,
  FETCH_QUESTIONS,
  MAKE_FORM,
  MAKE_FORM_SUCCESS } from './constants';

import {
  userLoaded,
  userFailed,
  questionsLoaded,
  questionsFailed,
  formLoaded,
  formFailed,
} from './actions';
import { Token } from 'react-bootstrap-typeahead';


export function* getUsers(data) {
  const requestURL = `${API_URL}${USER_API}?search=${data.param}`;
  try {
    const userResponse = yield call(request, requestURL);
    yield put(userLoaded(userResponse));
  } catch (err) {
    yield put(userFailed(err));
  }
}

export function* getQuestions() {
  const requestURL = `${API_URL}${QUESTION_API}`;
  try {
    const questionsResponse = yield call(request, requestURL);
    yield put(questionsLoaded(questionsResponse));
  } catch (err) {
    yield put(questionsFailed(err));
  }
}
function questionFormConverter(questions_obj, form_id)
{
    for (let key in questions_obj){
      questions_obj[key]['form_id'] = form_id
    }
    return questions_obj;
}

export function* makeForm(formInfo) {
  const requestURL = `${API_URL}${FORM_API}`;
  const formPost = {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json',
        }),
        body: JSON.stringify(formInfo.formValue),
  }
  try {
    const formResponse = yield call(request, requestURL,formPost);
    console.log(formResponse);
    /* Code for Question API call */
    var questionsFormatted = questionFormConverter(formInfo.questions,formResponse.id);
    console.log("Converted  ")
    const requestURLQuestions = `${API_URL}${ANSWERED_API}`;
    const questionPost = {
          method: 'POST',
          headers: new Headers({
            'Content-Type': 'application/json',
          }),
          body: JSON.stringify(questionsFormatted),
      }
    const questionsResponse = yield call(request, requestURLQuestions,questionPost );
    console.log(questionsResponse,"questions Response");
    yield put(formLoaded(questionsResponse))
    }
    catch (err) {
    yield put(formFailed(err));
  }
}

export default function* inventorySaga() {
  yield takeLatest(FETCH_USER_TITLE, getUsers);
  yield takeLatest(FETCH_QUESTIONS, getQuestions);
  yield takeLatest(MAKE_FORM, makeForm);
}
