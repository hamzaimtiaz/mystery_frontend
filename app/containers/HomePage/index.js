
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect } from 'react';
import FormInfo from 'components/formInfo'
import QuestionsForm from 'components/questionsForm';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { makeSelectUsers, makeSelectQuestions } from './selectors';
import { fetchUserTitles , fetchQuestions, makeForm } from './actions';
import { makeSelectAuth } from '../App/selectors'
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';
import {
  Form,
  Card,
  Button,
} from 'react-bootstrap';
const startfilter = '';
const key = 'forms';
const questions_form ={
  formName:``,
  user_id:0,
  date: new Date(),
  startTime: new Date().getTime(),
  endTime: new Date().getTime(),
  Questions: []
}


// eslint-disable-next-line react/prop-types
function homePage(
  { dispatchLoadUser,
    users,
    questions,
    dispatchLoadQuestions,
    dispatchMakeForm,
    auth,
    history 
  }) {

  auth.access ? '' : history.push('/');
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    dispatchLoadQuestions();
  }, []);

  
  const [form, setForm] = useState({
    formName:``,
    user_id:0,
    date: new Date(),
    startTime: new Date().getHours() + ":" + new Date().getMinutes(),
    endTime: new Date().getHours() + ":" + new Date().getMinutes(),
    Questions: [],
  });

  
  const handleOnChange = ev => {
    setForm({ ...form, [ev.target.name]: ev.target.value });
  };

  const handleOnDateChange = ev => {
    setForm({ ...form, date: ev});
  };

  const handleOnChangeQuestions = ev => {
    //Load form with total number of questions
    if(form.Questions.length == 0)
    {
      var temp1 = form.Questions;
      var i;
      for (i = 0; i < questions.length; i++) {
        temp1.push({question_id:0,answer: false, notes: '', marks:''});
      } 
      console.log(temp1,"Updated TEMP");
      setForm({...form,Questions:temp1})
    }

    //Check if Question ID doesnot exist; then update
    if(form.Questions[ev.target.id]['question_id']==0)
    {
    const temp3 = {...form.Questions[ev.target.id], question_id:ev.target.attributes.questionid.value};
    form.Questions[ev.target.id]=temp3
    }

    //Check if field changed is that of checkbox
    if([ev.target.name] == 'answer')
    {
      const temp2 = {...form.Questions[ev.target.id], [ev.target.name]:!form.Questions[ev.target.id][ev.target.name]};
      form.Questions[ev.target.id]=temp2
    }
    else
    {
      const temp4 = {...form.Questions[ev.target.id], [ev.target.name]:ev.target.value};
      form.Questions[ev.target.id]=temp4
    }
    
  };

  const handleOnTimeRange = (ev, type) => {
    setForm({ ...form, [type]: ev});
  };

  const handleOnChangeUser = ev => {
    setForm({ ...form, user_id: ev[0].id });
  };

  const InputChange = value => {
    dispatchLoadUser(value);
  };

  const handleSubmit = ev => {
   var formValue = {
      user_id: form.user_id,
      form_name: form.formName,
      file: null,
      call_start: form.startTime,
      call_end: form.endTime
  }
  console.log(form.Questions,"Form QUESTIONSSS")
    dispatchMakeForm(formValue,form.Questions);

  };


  return (
    <div className="homeStyle">
      <div>
        <FormInfo
          form={form}
          handleOnChange={handleOnChange}
          handleOnDateChange={handleOnDateChange}
          handleOnTimeRange={handleOnTimeRange}
          onInputChange={InputChange}
          users={users}
          handleOnChangeUser={handleOnChangeUser}
        />
      </div>
      <div>
      <Form>
        <Card>
          <Card.Body>
            <Card.Title>Questions</Card.Title>
        {questions.map((question, index) => 
        (
          <QuestionsForm
            form={form}
            handleOnChangeQuestions={handleOnChangeQuestions}
            index = {index}
            questions={question}
          />
        ))}
      </Card.Body>
        </Card>
      </Form> 
      </div>
      <div>
        
          <Button 
          variant="primary"
          onClick = {handleSubmit}
          >Submit</Button>
      </div>
      
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  auth : makeSelectAuth(),
  users: makeSelectUsers(),
  questions: makeSelectQuestions(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadUser: param => dispatch(fetchUserTitles(param)),
  dispatchLoadQuestions: () => dispatch(fetchQuestions()),
  dispatchMakeForm: (form_values,questions) => dispatch(makeForm(form_values,questions)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(homePage);
