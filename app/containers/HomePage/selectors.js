import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectMystery = state => state.forms || initialState;

// export const makeSelectInventoryResults = () =>
//   createSelector(
//     makeSelectInventory,
//     inventory => inventory.data,
//   );

// export const makeSelectGraphResults = () =>
//   createSelector(
//     makeSelectInventory,
//     inventory => inventory.graph,
//   );

// export const makeSelectIsLoading = () =>
//   createSelector(
//     makeSelectInventory,
//     inventory => inventory.isLoading,
//   );
export const makeSelectQuestions = () =>
  createSelector(
    makeSelectMystery,
    forms => forms.questions,
  );


export const makeSelectUsers = () =>
  createSelector(
    makeSelectMystery,
    forms => forms.users,
  );
