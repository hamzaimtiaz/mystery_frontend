import {
  FETCH_USER_TITLE,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILED,
  FETCH_QUESTIONS,
  FETCH_QUESTIONS_SUCCESS,
  FETCH_QUESTIONS_FAILED,
  MAKE_FORM,
  MAKE_FORM_FAILED,
  MAKE_FORM_SUCCESS,
  LOADING,
} from './constants';

export function makeForm(formValue,questions) {
  return {
    type: MAKE_FORM,
    formValue,questions,
  };
}
export function formLoaded(payload) {
  return {
    type: MAKE_FORM_SUCCESS,
    payload,
  };
}
export function formFailed(error) {
  return {
    type: MAKE_FORM_FAILED,
    error,
  };
}

export function fetchQuestions() {
  return {
    type: FETCH_QUESTIONS,
  };
}
export function questionsLoaded(payload) {
  return {
    type: FETCH_QUESTIONS_SUCCESS,
    payload,
  };
}
export function questionsFailed(error) {
  return {
    type: FETCH_QUESTIONS_FAILED,
    error,
  };
}

export function fetchUserTitles(param) {
  console.log('In actions')
  return {
    type: FETCH_USER_TITLE,
    param,
  };
}
export function userLoaded(payload) {
  return {
    type: FETCH_USER_SUCCESS,
    payload,
  };
}
export function userFailed(error) {
  return {
    type: FETCH_USER_FAILED,
    error,
  };
}
