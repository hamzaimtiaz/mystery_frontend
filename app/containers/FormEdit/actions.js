import {
  FETCH_USER_TITLE,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILED,
  MAKE_FORM,
  MAKE_FORM_FAILED,
  MAKE_FORM_SUCCESS,
  FETCH_QUESTION_INFO,
  QUESTION_INFO_SUCCESS,
  QUESTION_INFO_FAILED,
  LOADING,
} from './constants';

export function makeForm(formValue,questions) {
  return {
    type: MAKE_FORM,
    formValue,questions,
  };
}
export function formLoaded(payload) {
  return {
    type: MAKE_FORM_SUCCESS,
    payload,
  };
}
export function formFailed(error) {
  console.log(error,"Error in put")
  return {
    type: MAKE_FORM_FAILED,
    error,
  };
}

export function fetchUserTitles(param) {
  console.log('In actions')
  return {
    type: FETCH_USER_TITLE,
    param,
  };
}
export function userLoaded(payload) {
  return {
    type: FETCH_USER_SUCCESS,
    payload,
  };
}
export function userFailed(error) {
  return {
    type: FETCH_USER_FAILED,
    error,
  };
}

export function fetchQuestionInfo(param) {
  return {
    type: FETCH_QUESTION_INFO,
    param,
  };
}
export function questionInfoLoaded(payload) {
  return {
    type: QUESTION_INFO_SUCCESS,
    payload,
  };
}
export function questionInfoFailed(error) {
  console.log(error,"In actions error")
  return {
    type: QUESTION_INFO_FAILED,
    error,
  };
}
