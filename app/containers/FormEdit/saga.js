import { call, put, takeLatest } from 'redux-saga/effects';
import { API_URL, USER_API,QUESTION_API, FORM_API, ANSWERED_API } from '../../config';
import request from '../../utils/request';
import {
  FETCH_USER_TITLE,
  FETCH_QUESTION_INFO,
  MAKE_FORM,
 } from './constants';

import {
  userLoaded,
  userFailed,
  formLoaded,
  formFailed,
  questionInfoLoaded,
  questionInfoFailed,
} from './actions';
import { Token } from 'react-bootstrap-typeahead';


export function* getUsers(data) {
  const requestURL = `${API_URL}${USER_API}?search=${data.param}`;
  try {
    const userResponse = yield call(request, requestURL);
    yield put(userLoaded(userResponse));
  } catch (err) {
    yield put(userFailed(err));
  }
}

export function* makeForm(formInfo) {
  const requestURL = `${API_URL}${FORM_API}${formInfo.formValue.id}`;
  const formPost = {
        method: 'PUT',
        headers: new Headers({
          'Content-Type': 'application/json',
        }),
        body: JSON.stringify(formInfo.formValue),
  }
  try {
    console.log(`${API_URL}${FORM_API}${formInfo.formValue.id}`)
    const formResponse = yield call(request, requestURL,formPost);
    console.log(formResponse,"Update form");
    /* Code for Question API call */
    // var questionsFormatted = questionFormConverter(formInfo.questions,formResponse.id);
    // console.log("Converted  ")
    // const requestURLQuestions = `${API_URL}${ANSWERED_API}`;
    // const questionPost = {
    //       method: 'PUT',
    //       headers: new Headers({
    //         'Content-Type': 'application/json',
    //       }),
    //       body: JSON.stringify(questionsFormatted),
    //   }
    // const questionsResponse = yield call(request, requestURLQuestions,questionPost );
    // console.log(questionsResponse,"questions Response");
    // yield put(formLoaded(questionsResponse))
    }
    catch (err) {
    console.log("error")
    yield put(formFailed(err));
  }
}

export function* getQuestionsInfo(param) {
  const requestURL = `${API_URL}${ANSWERED_API}${`?form_id__id=`+param.param}`;
  try {
    const questionsResponse = yield call(request, requestURL);
    yield put(questionInfoLoaded(questionsResponse));
  } catch (err) {
    console.log("failed")
    yield put(questionInfoFailed(err));
  }
}

export default function* inventorySaga() {
  yield takeLatest(FETCH_USER_TITLE, getUsers);
  yield takeLatest(FETCH_QUESTION_INFO, getQuestionsInfo);
  yield takeLatest(MAKE_FORM, makeForm);
}
