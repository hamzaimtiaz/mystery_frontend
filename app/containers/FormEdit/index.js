
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect } from 'react';
import FormInfo from 'components/formInfo'
import QuestionsForm from 'components/questionsForm'
import { select } from 'redux-saga/effects';
import { selectLocationState } from 'containers/App/selectors';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { makeSelectUsers, makeSelectQuestions,makeSelectform } from './selectors';
import { fetchUserTitles , fetchQuestionInfo, makeForm} from './actions';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';
import {
  Form,
  Card,
  Button,
} from 'react-bootstrap';
const startfilter = '';
const key = 'forms';



// eslint-disable-next-line react/prop-types
function formUpdate(
  { dispatchLoadUser,
    users,
    questions,
    formData,
    dispatchLoadQuestions,
    dispatchMakeForm }
  
  ) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    dispatchLoadQuestions(formData.id);
  }, []);

  const [form, setForm] = useState({
    id:formData.id,
    formName:formData.form_name,
    user_id:formData.user_id,
    userName:formData.username,
    date: formData.date,
    startTime: formData.call_start,
    endTime: formData.call_end,
    Questions: [],
  });

  
  const handleOnChange = ev => {
    setForm({ ...form, [ev.target.name]: ev.target.value });
  };

  const handleOnDateChange = ev => {
    setForm({ ...form, date: ev});
  };

  const handleOnChangeQuestions = ev => {
    //Load form with total number of questions
    if(form.Questions.length == 0)
    {
      var temp1 = form.Questions;
      var i;
      for (i = 0; i < questions.length; i++) {
        temp1.push({question_id:0,answer: false, notes: '', marks:''});
      } 
      console.log(temp1,"Updated TEMP");
      setForm({...form,Questions:temp1})
    }

    //Check if Question ID doesnot exist; then update
    if(form.Questions[ev.target.id]['question_id']==0)
    {
    const temp3 = {...form.Questions[ev.target.id], question_id:ev.target.attributes.questionid.value};
    form.Questions[ev.target.id]=temp3
    }

    //Check if field changed is that of checkbox
    if([ev.target.name] == 'answer')
    {
      const temp2 = {...form.Questions[ev.target.id], [ev.target.name]:!form.Questions[ev.target.id][ev.target.name]};
      form.Questions[ev.target.id]=temp2
    }
    else
    {
      const temp4 = {...form.Questions[ev.target.id], [ev.target.name]:ev.target.value};
      form.Questions[ev.target.id]=temp4
    }
    
  };

  const handleOnTimeRange = (ev, type) => {
    setForm({ ...form, [type]: ev});
  };

  const handleOnChangeUser = ev => {
    console.log(ev,"In chane")
    if(ev){
      setForm({ ...form, user_id: ev[0].id});
      setForm({ ...form, userName: ev[0].username });
    }
    
  };

  const InputChange = value => {
    dispatchLoadUser(value);
  };

  const handleSubmit = ev => {
   var formValue = {
      id:form.id,
      user_id: form.user_id,
      form_name: form.formName,
      file: null,
      call_start: form.startTime,
      call_end: form.endTime

  }
    console.log(form,"New Form Values")
    dispatchMakeForm(formValue,questions);

  };
  return (
    <div className="homeStyle">
      <div>
        <FormInfo
          form={form}
          handleOnChange={handleOnChange}
          handleOnDateChange={handleOnDateChange}
          handleOnTimeRange={handleOnTimeRange}
          onInputChange={InputChange}
          users={users}
          handleOnChangeUser={handleOnChangeUser}
        />
      </div>
      <div>
      <Form>
        <Card>
          <Card.Body>
            <Card.Title>Questions</Card.Title>
        {questions.map((question, index) => 
        (
          <QuestionsForm
            form={form}
            handleOnChangeQuestions={handleOnChangeQuestions}
            index = {index}
            questions={question}
          />
        ))}
      </Card.Body>
        </Card>
      </Form> 
      </div>
      <div>
        
          <Button 
          variant="primary"
          onClick = {handleSubmit}
          >Submit</Button>
      </div>
      
    </div>
  );
        
}

const mapStateToProps = createStructuredSelector({
  formData:makeSelectform(),
  users: makeSelectUsers(),
  questions: makeSelectQuestions(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadUser: param => dispatch(fetchUserTitles(param)),
  dispatchLoadQuestions: param => dispatch(fetchQuestionInfo(param)),
  dispatchMakeForm: (form_values,questions) => dispatch(makeForm(form_values,questions)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(formUpdate);
