import produce from 'immer';
import { FETCH_USER_SUCCESS, QUESTION_INFO_SUCCESS } from './constants';

export const initialState = {
  data: {},
  graph: {},
  users: [],
  questions: [],
  isLoading: false,
};

export const routerState = {
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      
      
      case FETCH_USER_SUCCESS:
        draft.users = action.payload;
        break;
      case QUESTION_INFO_SUCCESS:
        console.log(action.payload,"questionare")
        draft.questions = action.payload;
        break;
     
    }
  });

export default homeReducer;
