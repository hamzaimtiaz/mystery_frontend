import { createSelector } from 'reselect';
import { initialState,routerState } from './reducer';

export const makeSelectMystery = state => state.forms || initialState;
export const makeSelectRouter = state => state.router || routerState;


export const makeSelectform = () =>
  createSelector(
    makeSelectRouter,
    forms => forms.location.state.currentForm,
  );

export const makeSelectQuestions = () =>

  createSelector(
  makeSelectMystery,
  forms => {
    if(forms.questions)
      return forms.questions
    return null 

  }
  
);

  

export const makeSelectUsers = () =>
  createSelector(
    makeSelectMystery,
    forms => forms.users,
  );
