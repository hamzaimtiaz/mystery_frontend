import { call, put, takeLatest } from 'redux-saga/effects';
import { SIGN_UP } from './constants';
// import { TOKEN_API, TOKEN_API_URL } from '../../assets/config';
import request from '../../utils/request';
import { signup_Success, signup_Failed } from './actions';

export function* signUpUser(signup_data) {
  const requestURL = `http://127.0.0.1:8000/api/user/`;
    // console.log(JSON.stringify(signup_data.payload));
  try {
    const signupResponse = yield call(request, requestURL, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(signup_data.payload),
    });
    console.log(signupResponse);
    yield put(signup_Success(signupResponse));
  } catch (err) {
      console.log(err,err.response);
    // yield put(signup_Failed(err));
  }
}

export default function* signupSaga() {
  yield takeLatest(SIGN_UP, signUpUser);
}
