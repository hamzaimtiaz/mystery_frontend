import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectUser = state => state.signup || initialState;

export const makeSelectResults = () =>
  createSelector(
    makeSelectUser,
    signup => {
      // console.log(makeSelectUser);
      // console.log(signup)
      return signup.authentication;
    },
  );
