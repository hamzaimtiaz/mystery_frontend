import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { signup } from './actions';
import { makeSelectResults } from './selector';
// import { makeSelectResults, makeSelectGraphResults } from './selectors';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';
const key = 'signup';
// import { makeSelectAuth } from '../App/selectors';
function Validate() {
  var password = document.getElementById("password").value;
  var confirmPassword = document.getElementById("repassword").value;
  if (password != confirmPassword) {
      alert("Passwords do not match.");
    return false;
  }
  return true;
}
function Signup({ dispatch_signup, signed_up }) {
  // authentication.access && history.push('/menu');
  // { dispatchLoadToken, history, authentication }
  // signed_up ? console.log('signedUp') : console.log('Not signed Up');
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const [signup_state, setsignup] = useState(
    {"username":"hamza","first_name":"hamza","last_name":"hamza","email":"hamza@test.com",
  "password":"123","repassword":"123"}
    );
  // setsignup({ ...signup_state, "is_admin": "false" });

  const handleOnChangeCheckbox = ev => {
    console.log(ev.target.checked)
    setsignup({ ...signup_state, [ev.target.name]: ev.target.checked });
  };
  const handleOnChange = ev => {
    setsignup({ ...signup_state, [ev.target.name]: ev.target.value });
  };

  const handleOnSubmit = () => {
    console.log(signup_state, 'SignupState');
    if(!signup_state.is_admin){setsignup({ ...signup_state, "is_admin": "false" });}
    if(Validate()){dispatch_signup(signup_state);}
    else console.log("Password donot match");
    

    // authentication.access && history.push('/menu');
  };

  return (
    <div id="signup">
      <h3 className="text-center text-white pt-5 login">Signup form</h3>
      <br />
      <br />
      <div className="container">
        <div
          id="signup-row"
          className="row justify-content-center align-items-center"
        >
          <div id="signup-column" className="col-md-6">
            <div id="signup-box" className="col-md-12">
              <form id="signup-form" className="form">
                <h3 className="text-center login">Signup</h3>
                <div className="form-group">
                  <label htmlFor="username" className="signup">
                    Username:
                  </label>
                  <br />
                  <input
                    type="text"
                    name="username"
                    id="username"
                    className="form-control"
                    onChange={handleOnChange}
                    value={signup_state.username}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="first_name" className="signup">
                    First Name:
                  </label>
                  <br />
                  <input
                    type="text"
                    name="first_name"
                    id="first_name  "
                    className="form-control"
                    onChange={handleOnChange}
                    value={signup_state.first_name}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="last_name" className="signup">
                    Last Name:
                  </label>
                  <br />
                  <input
                    type="text"
                    name="last_name"
                    id="last_name "
                    className="form-control"
                    onChange={handleOnChange}
                    value={signup_state.last_name}
                  />
                </div>


                <div className="form-group">
                  <label htmlFor="email" className="signup">
                    Email Address:
                  </label>
                  <br />
                  <input
                    type="email"
                    name="email"
                    id="email"
                    className="form-control"
                    onChange={handleOnChange}
                    value={signup_state.email}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="password" className="signup">
                    Password:
                  </label>
                  <br />
                  <input
                    type="password"
                    name="password"
                    id="password"
                    className="form-control"
                    onChange={handleOnChange}
                    value={signup_state.password}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="repassword" className="signup">
                    Confirm Password:
                  </label>
                  <br />
                  <input
                    type="password"
                    name="repassword"
                    id="repassword"
                    className="form-control"
                    value={signup_state.repassword}
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="is_admin" className="signup">
                    Is Admin:
                  </label>
                  <input 
                    type="checkbox"
                    name="is_admin"
                    id="is_admin"
                    class="defaultCheckbox"
                    onChange={handleOnChangeCheckbox}
                    value={signup_state.isChecked}
                  />
                </div>

                <div className="form-group">
                  <br />
                  <button
                    type="button"
                    name="submit"
                    className="btn btn-info btn-md login-btn"
                    onClick={handleOnSubmit}
                  >
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  signed_up: makeSelectResults(),
});

const mapDispatchToProps = dispatch => ({
  dispatch_signup: signup_data => dispatch(signup(signup_data)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(Signup);
