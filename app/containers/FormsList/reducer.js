import produce from 'immer';
import { FETCH_FORMS_SUCCESS } from './constants';

export const initialState = {
  allForms: [],
  isLoading: false,
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      // case FETCH_INVENTORY_SUCCESS:
      //   draft.data = action.payload;
      //   draft.isLoading = false;
      //   break;
      // case FETCH_GRAPH_SUCCESS:
      //   draft.graph = action.payload;
      //   break;

      case FETCH_FORMS_SUCCESS:
        draft.allForms = action.payload;
        break;
      // case FETCH_QUESTIONS_SUCCESS:
      //   draft.questions = action.payload;
      //   break;
      // case MAKE_FORM_SUCCESS:
      //   draft.questions = action.payload;
      //   break;

      // case LOADING:
      //   draft.isLoading = action.flag;
      //   break;
    }
  });

export default homeReducer;
