
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect } from 'react';
import { Link,  Redirect, withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';

import { fetchForms} from './actions';
import { fetchQuestionInfo} from '../App/actions';
import { makeSelectForms } from './selectors';
import {
  Table,
  // Card,
  // Button,
} from 'react-bootstrap';



const startfilter = '';
const key = 'forms';


// eslint-disable-next-line react/prop-types
function formPage(
  { dispatchLoadForms,
    dispatchLoadQuestionInfo,
    allForms }
  
  ) {

  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    dispatchLoadForms();
  }, []);

  
  const [form, setForm] = useState({
    //  forms:allForms,
    // user_id:0,
    // date: new Date(),
    // startTime: new Date(),
    // endTime: new Date(),
    // Questions: [],
  });

  
  const handleOnChange = ev => {
    setForm({ ...form, [ev.target.name]: ev.target.value });
  };


  const editSubmit = ev => {
  console.log(ev);
  // history = useHistory();
  // history.push(`/UpdateForm`);
  // dispatchLoadQuestionInfo(ev.id);
  };


  return (
    <div className="homeStyle">
      <div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Form Name</th>
            <th>User Name</th>
            <th>Date</th>
            <th>Call Start</th>
            <th>Call End</th>
            <th>Edit</th>
          </tr>
        </thead>
       
        <tbody>
          
          {allForms.map((currForm, index) => 
        ( 
          
        <tr key={index}>
        <td>{index}</td>
        <td >{currForm.form_name} </td>
        <td>{currForm.username}</td>
        <td>{currForm.date}</td>
        <td>{currForm.call_start}</td>
        <td>{currForm.call_end}</td>
        <td>
        <Link to={{
          pathname: '/UpdateForm',
          state: {
            currentForm: currForm
          }
        }}
        >
        <button 
          type="button"
          onClick = {() => editSubmit(currForm)}
          className="btn btn-primary"
          >Edit</button>
        </Link>

          
        </td>
        </tr>
        
       
        ))}
          </tbody>
         
      </Table>
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  allForms: makeSelectForms(),
  // questions: makeSelectQuestions(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadForms: () => dispatch(fetchForms()),
  // dispatchLoadQuestionInfo: form_name => dispatch(fetchQuestionInfo(form_name)),
  // dispatchLoadQuestions: () => dispatch(fetchQuestions()),
  // dispatchMakeForm: (form_values,questions) => dispatch(makeForm(form_values,questions)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withRouter,
)(formPage);
