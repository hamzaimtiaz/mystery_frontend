import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';

import { API_URL, FORM_API } from '../../config';
import {
  FETCH_USER_FORMS,FETCH_QUESTION_INFO } from './constants';
import {
  formsLoaded,
  formsFailed,
} from './actions';

export function* getForms() {
  const requestURL = `${API_URL}${FORM_API}`;
  try {
    const formsResponse = yield call(request, requestURL);
    yield put(formsLoaded(formsResponse));
  } catch (err) {
    yield put(formsFailed(err));
  }
}



export default function* inventorySaga() {
  yield takeLatest(FETCH_USER_FORMS, getForms);
  // yield takeLatest(MAKE_FORM, makeForm);
}
