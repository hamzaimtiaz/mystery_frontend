import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const makeSelectMystery = state => state.forms || initialState;


export const makeSelectForms = () =>
  createSelector(
    makeSelectMystery,
    forms => 
    {
      return forms.allForms
      
    }
    
  );


// export const makeSelectUsers = () =>
//   createSelector(
//     makeSelectMystery,
//     forms => forms.users,
//   );
