import {
  FETCH_USER_FORMS,
  FETCH_FORMS_SUCCESS,
  FETCH_FORMS_FAILED,
  FETCH_QUESTION_INFO,
  QUESTION_INFO_FAILED,
  QUESTION_INFO_SUCCESS,
  LOADING,
} from './constants';


  export function fetchForms() {
    return {
      type: FETCH_USER_FORMS,
    };
  }
  export function formsLoaded(payload) {
    return {
      type: FETCH_FORMS_SUCCESS,
      payload,
    };
  }
  export function formsFailed(error) {
    return {
      type: FETCH_FORMS_FAILED,
      error,
    };
}

