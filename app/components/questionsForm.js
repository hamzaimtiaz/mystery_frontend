import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Form,
  Row,
  Col,
  Card,
} from 'react-bootstrap';
import React, { useState, Fragment } from 'react';
import '../assets/css/main.css';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import NumericInput from 'react-numeric-input';


function QuestionsForm({
  handleOnChangeQuestions,
  questions,
  index,
}) {
  
  return (
    <div>
      <Row>
        <Form.Label class="input-group mb-3" controlId="formGridFormIndex">
        <label name>{index + 1}</label>
        </Form.Label>
        <Form.Group as={Col} controlId="formGridFormQuestion">
          <Form.Control plaintext readOnly value = {questions.question} />
        </Form.Group>
        <Form.Group as={Col} controlId="formGridFormNotes">
          <input
            type="entry"
            className="form-control"
            id = {index}
            questionid = {questions.id}
            value = {questions.notes}
            name="notes"
            placeholder="Notes"
            onChange={handleOnChangeQuestions}
          />
        </Form.Group>
        <Form.Group as={Col} controlId="formGridFormMarks" onChange={handleOnChangeQuestions}>
          <NumericInput
            className="form-control"
            id = {index}
            questionid = {questions.id}
            value = {questions.marks}   
            name="marks"
            placeholder="Marks"
            // onChange={handleOnChangeQuestions}
            strict
          />
        </Form.Group>
        <Form.Group as={Col} controlId={index} onChange = {handleOnChangeQuestions}>
          
          <Form.Check
            type="checkbox"
            name="answer"
            label="Answered"
            // onChange={handleOnChange}
          />
        </Form.Group>
      </Row>
          
    </div>
  );
}
export default QuestionsForm;