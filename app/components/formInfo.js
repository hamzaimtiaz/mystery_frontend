import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Form,
  Row,
  Col,
  Card,
  // Badge,
  // Button,
  // FormGroup,
  // FormControl,
  ControlLabel,
} from 'react-bootstrap';
import React, { useState, Fragment } from 'react';
import '../assets/css/main.css';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import TimePicker from 'react-time-picker';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';


function FormInfo({
  form,
  handleOnChange,
  handleOnTimeRange,
  handleOnDateChange,
  onInputChange,
  users,
  handleOnChangeUser,
}) {
  const userOptions = {
    allowNew: false,
    isLoading: false,
    multiple: false,
    options: users,
  };
  const handleOnStartTime = ev => {
    form.startTime = ev
    handleOnTimeRange(ev, 'startTime');
  };

  const handleOnEndTime = ev => {
    form.endTime = ev
    handleOnTimeRange(ev, 'endTime');
  };
  return (
    <div>
      <Form>
        <Card>
          <Card.Body>
            <Card.Title>Form Input</Card.Title>
            <Row>
              <Form.Group as={Col} controlId="formGridFormName">
                <Form.Label>Form Name</Form.Label>
                <input
                  type="entry"
                  className="form-control"
                  id="formName"
                  value = {form.formName}
                  name = "formName"
                  placeholder="Text"
                  onChange={handleOnChange}
                />
              </Form.Group>
              <Form.Group as={Col} controlId="formGridDate">
                <Form.Label>Date</Form.Label>
                <DatePicker
                  name="date"
                  onChange={handleOnDateChange}
                  dateFormat="yyyy-MM-dd"
                  selected={Date.parse(form.date)}
                  className="form-control"
                />
              </Form.Group>
              <Form.Group as={Col} controlId="formGridStartTime">
                <Form.Label>Start Time</Form.Label>
                <TimePicker
                  format="HH:mm"
                  name="startTime"
                  onChange={handleOnStartTime}
                  value={form.startTime}
                  className="form-control"
                />
              </Form.Group>
              <Form.Group as={Col} controlId="formGridEndTime">
                <Form.Label>End Time</Form.Label>
                <TimePicker
                  format="HH:mm"
                  name="endTime"
                  onChange={handleOnEndTime}
                  value={form.endTime}
                  className="form-control"
                />
              </Form.Group>
              <Form.Group as={Col} controlId="formGridEndDate">
                <Form.Label>User Name</Form.Label>
                <Fragment>
                  <AsyncTypeahead
                    {...userOptions}
                    labelKey={option => `${option.username}`}
                    id = "userName"
                    defaultInputValue={form.userName}
                    placeholder="Choose a user..."
                    onSearch={onInputChange}
                    onChange={handleOnChangeUser}
                  />
                </Fragment>
              </Form.Group>
            </Row>
          </Card.Body>
        </Card>
      </Form>
    </div>
  );
}
export default FormInfo;
