export const API_URL = 'http://127.0.0.1:8000/api/';
export const USER_API = 'user/';
export const TOKEN_API = 'token/';
export const QUESTION_API = 'question/';
export const FORM_API = 'form/';
export const ANSWERED_API = 'answered/';
